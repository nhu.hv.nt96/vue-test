const getters = {
    unread: (state) => state.messages.filter((message) => !message.read),
    unreadFrom: (state, getters) => getters.unread
        .map((message) => message.user.name)
}

export default getters