import Vue from 'vue'
import Router from 'vue-router'
import PageHome from '@/components/PageHome'
import PageAbout from '@/components/PageAbout'
import PageUser from '@/components/PageUser'
import PageSettings from '@/components/page/PageSettings'
import PageSettingsProfile from '@/components/page/PageSettingsProfile'
import PageSettingsEmail from '@/components/page/PageSettingsEmail'


Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'PageHome',
      component: PageHome
      // redirect: '/preferences'
    },
    {
      path: '/about',
      component: PageAbout,
      meta: {
        title: 'Welcome to my blog!'
      }
    },
    {
      path: '/user/:userId',
      component: PageUser
    },
    {
      path: '/settings',
      component: PageSettings,
      alias: '/preferences',
      children: [
        {
          path: 'profile',
          component: PageSettingsProfile,
        },
        {
          path: 'email',
          component: PageSettingsEmail,
        }
      ]
    },
    // {
    //   path: '*',
    //   component: PageNotFound
    // }
  ]
})

router.beforeEach((to, from, next) => {
  // const requiresAuth = to.matched.some((record) => {
  //   return record.meta.requiresAuth;
  // }
  //   if (requiresAuth && !userAuthenticated()) {
  //   next('/login')
  // } else {
  //   next();
  // }
  next()
})

router.afterEach((to) => {
  document.title = to.meta.title;
});


export default router